# Lightning Grid
This repository contains a Lightning component that can dynamically render a grid given an SObject type, a set of records and a list of fields to show. This grid is meant as a base component and is to be used within other components, a sample host component has been included called "ContactGrid" which can be put on Account record pages and will show all Account contacts.  

![grid.png](https://bitbucket.org/repo/Eg6x4Ra/images/2085362923-grid.png)

## Building
To build and deploy this repository you must have [Apache Ant](http://ant.apache.org/) and Java installed. When in the root of the repository you can use the `ant` command in the terminal to run build targets. Before you can push the package to a Salesforce org you must create a `local.build.properties` file in the root of the repository, copy this code into it;

```
# ==============================================================================
# =========================== Local Build Properties ===========================
# ==============================================================================
# Use this file to set build properties that will not get committed to source
# control as this file is gitignored. Comment a line out to recieve a default
# value from the build.properties file.

# ============================== Salesforce ====================================
# For a production org use: https://login.salesforce.com
# For a sandbox org use: https://test.salesforce.com
sf.serverurl = https://login.salesforce.com

sf.username = <Insert your Salesforce username here>
sf.password = <Insert your Salesforce password here>
sf.token = <Insert your Salesforce security token here>
```

Substitute your account username, password and security token in the specified areas of the file. Once your login details are in the build properties (don't worry they won't be committed) you can then use Ant to push and pull changes from the org.

To push the package to your org type the command `ant deploy` at the command line. Below is a list of targets that can be used with ant in this project;

* `ant undeploy` - Deletes the package components from your org.
* `ant update.metadata` - Updates the `package.xml` manifest with new metadata components.
* `ant retrieve.metadata` - Retrieves the package source code from the org.
* `ant new.class -Dname=ClassName` - Creates a new Apex class with the specified class name.
* `ant new.lightning -Dname=Component -Ddescription=Thing` - Creates a new Lightning component bundle with the given name and description

## Example Component
Below is an example of how this grid can be used to create a grid to show Opportunity Products;

**Component**
```xml
<aura:component controller="OpportunityLineController" implements="flexipage:availableForRecordHome,force:hasRecordId" access="global">
	<aura:attribute name="recordId" type="String" description="The ID of the current Opportunity" />
	<aura:attribute name="products" type="Object[]" default="Product2Id,ProductCode,Description" description="The products to show" />
	<aura:attribute name="fields" type="String[]" description="The columns to display in the grid" />

	<aura:handler name="init" value="{!this}" action="onInit" />

	<lightning:card iconName="standard:product_item" title="Opportunity Products">
        <div class="slds-is-relative">
            <c:Grid type="OpportunityLineItem" records="{!v.products}" fields="{!v.fields}" />

            <lightning:spinner aura:id="spinner" variant="brand" size="large"/>
        </div>
	</lightning:card>
</aura:component>
```

**Client Controller**
```javascript
({
	onInit: function ( component, event, helper ) {
		var getOpportunityLines = component.get('c.getOpportunityLines'),
			recordId = component.get('v.recordId'),
			fields = component.get('v.fields');

		getOpportunityLines.setParams.({ recordId: recordId, fields: fields.join() });
		getOpportunityLines.setCallback( function ( response ) {
			if ( response.getState() !== 'SUCCESS' ) {
				// TODO: Handle error
				return;
			}

			component.set('v.products', response.getReturnValue());
		});

		$A.enqueueAction(getOpportunityLines);
	}
})
```

**Apex Controller**
```java
global with sharing class OpportunityLineController
{
	@AuraEnabled
    global static List<Contact> getOpportunityLines(String recordId, String fields)
    {
        String query = String.format(
            'SELECT Id, {0} FROM OpportunityLineItem WHERE OpportunityId = {1} LIMIT 10000',
            new List<String>{
                fields,
                quoteString(recordId)
            }
        );

        return Database.query(query);
    }

    private static String quoteString(String value)
    {
		return '\'' + value + '\'';
    }
}
```