module Salesforce
	extend ::RSpec::Matchers
	extend Capybara::DSL

	module Login
		URL = 'https://login.salesforce.com'
		USERNAME_LABEL = 'username'
		PASSWORD_LABEL = 'password'
		LOGIN_BUTTON_LABEL = 'Login'
	end

	module Apps
		SALES = 'Sales'
	end

	module Tabs
		ACCOUNTS = 'Accounts'
	end

	# Navigates to the Salesforce login page and logs into an org with the
	# given username and password.
	# +username+:: The username to login with
	# +password+:: The password for the user
	def Salesforce.login username, password
		visit Login::URL

		fill_in Login::USERNAME_LABEL, with: username
		fill_in Login::PASSWORD_LABEL, with: password

		click_button Login::LOGIN_BUTTON_LABEL
	end

	# Navigates to the specified app.
	# +name+:: The name of the app to navigate to
	def Salesforce.app name
		# TODO: Navigate to an app
	end

	# Navigates to the specified tab from the all tabs page.
	# +name+:: The name of the tab to naviagte to
	def Salesforce.tab name
		# TODO: Navigate to a tab
	end

	# Invokes some anonymous Apex
	# +code+:: The Apex code to execute
	def Salesforce.execute apex
		# TODO: Invoke Ant to perform anonymous Apex
	end

	module ListView

		#Clicks the 'new' button on a list view page.
		def ListView.create
			# TODO: Click list view 'new' button
		end

		# Navigates to the specified list view from the tab home.
		# +name+:: The name of the list view to navigate to
		def ListView.go name
			# TODO: Goto a specific list view
		end

		# Views the first record that is found when looking for the specified target
		# text.
		# +target+:: The text to find the record by
		def ListView.view target
			# TODO: Goto the view page for a record with the target text on it's row
		end
	end

	module DetailView

		# Clicks the 'save' button.
		def DetailView.save
			# TODO: Click the save button
		end

		# Clicks the 'edit' button.
		def DetailView.edit
			# TODO: Click the edit button
		end

		# Clicks the 'delete' button.
		def DetailView.delete
			# TODO: Click the delete button
		end

		# Gets the specified field from the view and returns it.
		# +field+:: The label of the field whose value is to be returned
		def DetailView.get field
			# TODO: Get the value of the specified field
		end
	end
end

################################ RSpec contexts ###############################
shared_context 'logged_in' do
	before(:all) do
		settings = Build.settings

		Salesforce.login settings.username, settings.password
	end
end
