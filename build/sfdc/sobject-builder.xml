<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" xmlns:ac="net.sf.antcontrib" xmlns:sobject="com.salesforce.schema" name="sobject-builder">

	<!-- Example usage -->
	<target name="build.example.sobject" description="Example SObject builder usage">
		<sobject:new package="${build.src}" name="MyObject__c">
			<content>
				<sobject:label singular="My Object" plural="My Objects" />
				<sobject:description value="My very own SObject!" />
				<sobject:namefield type="AutoNumber" label="Object Number" format="MO{000000}" />

				<sobject:field name="MyField__c" label="My Field" type="Text">
					<field-content>
						<sobject:field-description value="My very own SObject field!" />
						<sobject:field-length value="255" />
						<sobject:field-required value="false" />
					</field-content>
				</sobject:field>

				<sobject:field name="MyFormula__c" label="My Formula" type="Text">
					<field-content>
						<sobject:field-description value="My very own SObject formula field!" />
						<sobject:field-length value="255" />
						<sobject:field-formula code="IF( ISNULL(MyField__c), 'Field is Null!', MyField__c )" />
					</field-content>
				</sobject:field>

				<sobject:sharing-model model="FullAccess" />
			</content>
		</sobject:new>
	</target>

 	<macrodef name="new" description="Creates a new SObject metadata file" uri="com.salesforce.schema">
		<attribute name="package" description="The path to the package directory to add the SObject to" />
		<attribute name="name" description="The API name of the SObject" />
		<attribute name="defaults" default="true" description="When true various features are defaulted to on" />
		<element name="content" description="Contains tasks that can add content like fields to the metadata file" />
		<sequential>
			<!-- Register filename variable and write metadata file header -->
			<ac:var name="sobject.filename" value="@{package}/objects/@{name}.object" />
			<echo file="${sobject.filename}">&lt;?xml version="1.0" encoding="UTF-8"?&gt;&#xA;&lt;CustomObject xmlns="http://soap.sforce.com/2006/04/metadata"&gt;&#xA;</echo>

			<!-- Allow caller to inject metadata -->
			<content />

			<!-- Default various options if desired -->
			<ac:if><equals arg1="@{defaults}" arg2="true" />
				<then>
					<sobject:element name="allowInChatterGroups" value="true" />
					<sobject:element name="compactLayoutAssignment" value="SYSTEM" />
					<sobject:element name="deploymentStatus" value="Deployed" />
					<sobject:element name="enableActivities" value="true" />
					<sobject:element name="enableBulkApi" value="true" />
					<sobject:element name="enableFeeds" value="false" />
					<sobject:element name="enableHistory" value="false" />
					<sobject:element name="enableReports" value="true" />
					<sobject:element name="enableSearch" value="true" />
					<sobject:element name="enableSharing" value="true" />
					<sobject:element name="enableStreamingApi" value="true" />
				</then>
			</ac:if>

			<!-- Write file footer -->
			<echo file="${sobject.filename}" append="true">&lt;/CustomObject&gt;</echo>
		</sequential>
	</macrodef>

	<macrodef name="line" description="Adds a line to the SObject metadata" uri="com.salesforce.schema">
		<attribute name="content" description="The content to add to the metadata file" />
		<sequential>
			<echo file="${sobject.filename}" append="true">&#009;@{content}&#xA;</echo>
		</sequential>
	</macrodef>

	<macrodef name="element" description="Adds an element to the SObject metadata" uri="com.salesforce.schema">
		<attribute name="name" description="The name of the metadata element" />
		<attribute name="value" description="The value to set the node to" />
		<attribute name="child" default="false" description="If true the element will be indented more" />
		<sequential>
			<ac:if><equals arg1="@{child}" arg2="true" />
				<then>
					<sobject:sub-line content="&lt;@{name}&gt;@{value}&lt;/@{name}&gt;" />
				</then>
				<else>
					<sobject:line content="&lt;@{name}&gt;@{value}&lt;/@{name}&gt;" />
				</else>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="empty-element" description="Adds a self closing element to the SObject metadata" uri="com.salesforce.schema">
		<attribute name="name" description="The name of the metadata element" />
		<attribute name="child" default="false" description="If true the element will be indented more" />
		<sequential>
			<ac:if><equals arg1="@{child}" arg2="true" />
				<then>
					<sobject:sub-line content="&lt;@{name} /&gt;" />
				</then>
				<else>
					<sobject:line content="&lt;@{name} /&gt;" />
				</else>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="sub-line" description="Adds a further indented line to the SObject metadata" uri="com.salesforce.schema">
		<attribute name="content" description="The content to add to the metadata file" />
		<sequential>
			<echo file="${sobject.filename}" append="true">&#009;&#009;@{content}&#xA;</echo>
		</sequential>
	</macrodef>

	<macrodef name="label" description="Adds a label to a SObject metadata file" uri="com.salesforce.schema">
		<attribute name="singular" description="The singular form for the label" />
		<attribute name="plural" description="The plural form for the label" />
		<sequential>
			<sobject:element name="label" value="@{singular}" />
			<sobject:element name="pluralLabel" value="@{plural}" />
		</sequential>
	</macrodef>

	<macrodef name="description" description="Adds a description to a SObject metadata file" uri="com.salesforce.schema">
		<attribute name="value" description="The description text to set" />
		<sequential>
			<sobject:element name="description" value="@{value}" />
		</sequential>
	</macrodef>

	<macrodef name="namefield" description="Adds a name field to the SObject" uri="com.salesforce.schema">
		<attribute name="type" description="The type of name field (AutoNumber, Text)" />
		<attribute name="format" default="" description="The autonumber display format, like &quot;MO{000000}&quot;" />
		<attribute name="label" description="The field label" />
		<sequential>
			<sobject:line content="&lt;namefield&gt;" />

			<ac:if><equals arg1="@{type}" arg2="AutoNumber" />
				<then>
					<sobject:element name="displayFormat" value="@{format}" child="true" />
				</then>
			</ac:if>

			<sobject:element name="label" value="@{label}" child="true" />
			<sobject:element name="type" value="@{type}" child="true" />

			<sobject:line content="&lt;/namefield&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="sharing-model" description="Adds a sharing model" uri="com.salesforce.schema">
		<attribute name="model" description="The sharing model to use (Private, Read, ReadWrite, ReadWriteTransfer, FullAccess, ControlledByParent)" />
		<sequential>
			<sobject:element name="sharingModel" value="@{model}" />
		</sequential>
	</macrodef>

	<macrodef name="field" description="Adds a field to the SObject" uri="com.salesforce.schema">
		<attribute name="name" description="The API name of the field" />
		<attribute name="label" description="The label for the field" />
		<attribute name="type" description="The data type of the field (AutoNumber, Lookup, MasterDetail, MetadataRelationship, Checkbox, Currency, Date, DateTime, Email, EncryptedText, ExternalLookup, IndirectLookup, Number, Percent, Phone, Picklist, MultiselectPicklist, Summary, Text, TextArea, LongTextArea, Summary, Url, Hierarchy, File, CustomDataType, Html, Geolocation)" />
		<element name="field-content" optional="true" description="Contains extra field options" />
		<sequential>
			<sobject:line content="&lt;fields&gt;" />

			<sobject:element name="fullName" value="@{name}" child="true" />
			<sobject:element name="label" value="@{label}" child="true" />

			<field-content />

			<sobject:element name="type" value="@{type}" child="true" />

			<sobject:line content="&lt;/fields&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="field-description" description="Adds a description to the field definition" uri="com.salesforce.schema">
		<attribute name="value" description="The value of the description" />
		<sequential>
			<sobject:element name="description" value="@{value}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="field-length" description="Adds a length to the field definition" uri="com.salesforce.schema">
		<attribute name="value" description="The value of the length" />
		<sequential>
			<sobject:element name="length" value="@{value}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="field-required" description="Specifies if the field is required" uri="com.salesforce.schema">
		<attribute name="value" description="true if required, false otherwise" />
		<sequential>
			<sobject:element name="required" value="@{value}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="field-help" description="Specifies the inline help text for the field" uri="com.salesforce.schema">
		<attribute name="value" description="true if required, false otherwise" />
		<sequential>
			<sobject:element name="inlineHelpText" value="@{value}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="field-formula" description="Specifies a formula to compute the field value" uri="com.salesforce.schema">
		<attribute name="code" description="The source code for the formula" />
		<attribute name="blanks" default="BlankAsZero" description="How to handle blanks in the formula" />
		<sequential>
			<sobject:element name="formula" value="@{code}" child="true" />
			<sobject:element name="formulaTreatBlanksAs" value="@{blanks}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="field-lookup" description="Specifies what SObject this field looks up to" uri="com.salesforce.schema">
		<attribute name="name" description="The API name for this relationship" />
		<attribute name="target" description="The SObject that this field looks up to" />
		<attribute name="label" description="The label for this relationship" />
		<attribute name="deleteConstraint" default="SetNull" description="The delete constraint for this lookup (SetNull, Restrict, Cascade)" />
		<sequential>
			<sobject:element name="deleteConstraint" value="@{deleteConstraint}" child="true" />
			<sobject:element name="referenceTo" value="@{target}" child="true" />
			<sobject:element name="relationshipLabel" value="@{label}" child="true" />
			<sobject:element name="relationshipName" value="@{name}" child="true" />
		</sequential>
	</macrodef>

	<macrodef name="property-field" description="Creates a field from the current values of properties field.*, to be used within a loop" uri="com.salesforce.schema">
		<sequential>
			<ac:switch value="${field.type}">
				<!-- TODO: Add more field types -->
				<case value="Currency">
					<sobject:element name="precision" value="${field.precision}" />
					<sobject:element name="scale" value="${field.scale}" />
				</case>

				<default><!-- Unknown type, do nothing --></default>
			</ac:switch>
		</sequential>
	</macrodef>
</project>
