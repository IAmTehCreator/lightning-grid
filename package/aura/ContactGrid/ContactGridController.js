({
    onAccountChange : function(component, event, helper) {
		var getContacts = component.get('c.getContacts'),
            accountId = component.get('v.recordId'),
            spinner = component.find('spinner'),
            fields = component.get('v.fields');
        
        $A.util.removeClass(spinner, "slds-hide");
        
        getContacts.setParams({ accountId: accountId, fields: fields.join() });
        getContacts.setCallback(this, function (response) {
            var state = response.getState();

            switch ( state ) {
                case 'SUCCESS':
                	component.set( 'v.contacts', response.getReturnValue() );
                	break;
                case 'ERROR':
                    console.log('Error: ', response.getError()[0].message);
                    break;
                default:
                    console.log('Error: Unknown state');
            }
            
            $A.util.addClass(spinner, "slds-hide");
        });

        $A.enqueueAction(getContacts);
	}
})