({
	onTypeChange : function(component, event, helper) {
		var getFieldDescribes = component.get('c.getFieldDescribes'),
            sObjectType = component.get('v.type');
        
        getFieldDescribes.setParams({ sObjectType: sObjectType });
        getFieldDescribes.setCallback(this, function ( response ) {
            var state = response.getState(),
                describes,
                records,
                fields;

            switch ( state ) {
                case 'SUCCESS':
                    describes = JSON.parse( response.getReturnValue() );
                    records = component.get('v.records');
                    fields = component.get('v.fields');

                    component.set('v.columns', helper.getColumns(fields, describes.fields));

                    records = helper.prepareRecords(fields, describes.fields, records);
                    component.set('v.values', records);
                	break;
                case 'ERROR':
                    console.log('Error: ', response.getError()[0].message);
                    break;
                default:
                    console.log('Error: Unknown state');
            }
        });
        
        $A.enqueueAction(getFieldDescribes);
	}
})