({    
    getColumns: function ( fieldOrder, describes ) {
        var columns = [],
            field,
            i;

        for ( i = 0; i < fieldOrder.length; i++ ) {
            field = fieldOrder[i];

            columns.push( describes[field] );
        }
        
        return columns;
    },

    prepareRecords: function ( fieldOrder, describes, records ) {
        var ordered = [],
            fieldArray,
            describe,
            record,
            values,
            value,
            field,
            i,
            j;
       	
        for ( i = 0; i < records.length; i++ ) {
            record = records[i];
        	
            fieldArray = [];
            for ( j = 0; j < fieldOrder.length; j++ ) {
                field = fieldOrder[j];
                
                describe = describes[field];
                value = record[field];
                
                fieldArray.push({
                    name: describe.name,
                    label: describe.label,
                    type: describe.dataType,
                    value: value
                });
            }
            
            ordered.push(fieldArray);
        }
        
        return ordered;
    }
})