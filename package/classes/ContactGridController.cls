global with sharing class ContactGridController
{
	@AuraEnabled
    global static List<Contact> getContacts(String accountId, String fields)
    {
        String query = String.format(
            'SELECT Id, {0} FROM Contact WHERE AccountId = {1} LIMIT 10000',
            new List<String>{
                fields,
                quoteString(accountId)
            }
        );

        return Database.query(query);
    }
    
    private static String quoteString(String value)
    {
		return '\'' + value + '\'';
    }
}