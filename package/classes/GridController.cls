global with sharing class GridController
{
    public static final Map<Schema.SoapType, String> TYPES = new Map<Schema.SoapType, String>{
        Schema.SoapType.Boolean => 'boolean',
        Schema.SoapType.Double => 'double',
        Schema.SoapType.Date => 'date',
        Schema.SoapType.Id => 'id',
        Schema.SoapType.Integer => 'integer',
        Schema.SoapType.String => 'string'
    };

	@AuraEnabled
    global static String getFieldDescribes(String sObjectType)
    {
        FieldDescribes describes = new FieldDescribes();
        
        DescribeSObjectResult describe = getDescribeForSObject(sObjectType);
        Map<String, SObjectField> fields = describe.fields.getMap();

        for(String field : fields.keySet())
        {
			DescribeFieldResult fieldDescribe = fields.get(field).getDescribe();
            describes.add(fieldDescribe);
        }
        
        return JSON.serialize(describes);
	}
    
    private static DescribeSObjectResult getDescribeForSObject(String sObjectType)
    {
        return Schema.getGlobalDescribe().get(sObjectType).getDescribe();
    }
    
    global class FieldDescribes
    {
        global Map<String, FieldDescribe> fields;
        
        global FieldDescribes()
        {
            fields = new Map<String, FieldDescribe>();
        }
        
        global void add(Schema.DescribeFieldResult describe)
        {
            fields.put(describe.getName(), new FieldDescribe(describe));
        }
    }
    
    global class FieldDescribe
    {
        global String name;
        global String label;
        global String dataType;
        
        global FieldDescribe(Schema.DescribeFieldResult describe)
        {
            name = describe.getName();
            label = describe.getLabel();
            dataType = stringifyType(describe.getSoapType());
        }
        
        private String stringifyType(Schema.SoapType soapType)
        {
            return TYPES.get(soapType);
        }
    }
}